# Format des données

## Quelles données va-t-on fouiller ?

* Comme on l'a vu dans le chapitre précédent, la fouille de données va étudier un ensemble d'objets afin de répondre à la question posée. Les objets coollecté en vue de l'étude sont décrits par un ensemble de propétés qui permmetten de la décrire
  * Par exemple, un client qui sollicite un prêt sera décrit par ses caractéristiques administrative (âge, situation familliale, ...), socio économiques (catégorie socio-professionnelle, revenus, patrimoine), bancaire...
* Le choix de la réprésentation d'un objet peut, dans certains cas, être une question en soi.

## Données manipulées

* Même si l'on dispose d'un entrepôt de données ou d'une base de donnée "classique" où les données sont stockéees dans différentes table, le processus de data mining s'applique sur **un seul tabealu de données**

### Tableau de données

Chaque colonne décrit

* Une variable
* Un attribut
* Un descripteur
* Un champ

Chaque ligne décrit

* Un individu (sens statistique)
* Un exemple
* Un cas
* Un enregistrement

### Codage et type des variables

* Les informations manipuléées peuvent être des données quantitatives (nombres), des données qualitatives (des valeurs symboliques), das valeurs booléennes, des textes...mais aussi des arbres (ex : document xml), des graphes (ex: molécules), des séries temporelles (signal mesuré à différents instants)...
* Le codage des informations est important
  * _Ex : salaire codé par une valeur numérique ou par les valeur `{Faible, Moyen, Élevé}`_
* On distingue ainsi plusieurs types de variables
* Certaines méthodes de data mining ne sont utilisables que sur certains types de données

#### Type de variables

* Variables continue ou numérique : les valeurs sont un sous ensemble infini de R
  * _Ex: Salaire_
* Variables discrètes : les valeurs sont un sous-ensemble fini de N
  * _Ex: nombre d'enfants_
* Variabke qualitative ou catégorielle symbolique : les valeurs sont des symboles ou des codes numériques qui ne représentent pas des quantité
  * _Ex: Bleu, rouge, 49 (num departement)_
  * Pas d'opération mathématiques
  * Pas d'ordre sur les valeurs sauf dans le cas d'une **variable ordinale**  
    * _ex : Salaire : Faible \< Moyen \< Élevé_

Le codage est important !

#### Discrétisation

Passage d'une donnée continue à une donnée oridinale

La discrétisation constiste à transformer une variable continue en variable discète en découpant son domaine de variation en intervalles

* _Exemple :salaire ∈ \[500, 150000\] transformé en une variable à trois valeurs {Faible, Moyen, Élevé}._
* Il faut choisir les seuils de découpage de l'intervalle en fonction de connaissances du domaine
* Remarque : on verra que la méthode des arbres de décision propose une discrétisation automatique, pendant la construction de l'arbre.

#### Donnée qualitative ➡ Donnée continue

* On remplace les valeurs symboliques d'une variable par plusieurs variables booléennes (codage dusjonctif)
* _Exemple :remboursement ∈ {Lent, Normal, Rapide}, remplacé par
  * 0 pour lent
  * 0,5 pour normal
  * 1 pour rapide
* _Exemple 2 : Situation familliale ∈ {Célibataire, Marié, Divorcé}, remplacé par :_
  * _Sit\_celibataire ∈ {0,1}_
  * _Sit\_marié ∈ {0,1}_
  * _Sit\_divorcé ∈ {0,1}_
* On perd potentiellement l'ordre

#### Donnée continue ➡ Donnée continue

* La normalisation consite à ramener les valeurs d'une variable continue dans un intervalle donné (par ex \[0; 1\])
  * _Ex : V → (V - min(V))/(max(V) - min(V)) ramène les valeurs dans l'intervalle \[0;1\]_
* La normalisation permet de travailler avec des variables qui ont des ordres de grandeur très différents
* On peut aussi agir sur la distribution des valeurs
  * _Ex : V → log(V)_

#### Autre transformations possibles

* En géomarketing
  * Code postal → Lieu
