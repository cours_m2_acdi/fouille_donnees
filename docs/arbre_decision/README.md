# Arbre de décision

* Tâche de data mining supervisé, prédictif
* On présente ici les arbres pour la classification
* A partir de l’étude d’un jeu d’exemples dont on connaît la classe, on construit un arbre de décision qui pour un nouvel exemple permet de prédire sa classe
  * Reconnaissance SPAM/Non SPAM
  * Risque de crédit

## Représentation par arbre de décision

On considère tout d’abord le cas d’attributs nominaux ou discrets

* Chaque noeud interne test un attribut
* Chaque branche correspond à une valeur d'attribut
* Chaque feuille correspond à une classe unique (décision oui ou décision non) ou à une classe majoritaire

## Entropie

Entropie(S) = Nombre de bits nécessaire pour coder la classe 
(+ ou -) d’un élément tiré au hasard

## Critère d'arrêt

* Un noeud est une feuille si
  * Tous les exemples associés à ce noeud sont de la même classe : classe de la feuille
  * Si aucun test ne peut être sélectionné : classe majoritaire
  * Attribuer à un noeud la classe majoritaire des exemples qu'il contient si le nombre d'exemple est faible, si la profondeur maximum souhaité est atteinte, ... → paramétrage de la méthode
* Si un nœud ne contient aucun exemple, on lui attribue la classe majoritaire du père

## Surajustement

**Surajustement (sur-apprentissage)** arbre trop proche des données d'apprentissage: Arbre peu performant pour la prédiction

* Définition: un hypothèse h surajuste les données d’apprentissage S s’il existe une autre hypothèse h’ tq `erreur S(h) < erreur S(h’)` et `erreur P(h) > erreur P(h’)`  (h meilleure que h' sur l'échantillon d'apprentissage mais pas sur l'ensemble de la population)

* Le bruit dans les exemples peut conduire à un surajustement de l’arbre