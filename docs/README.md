# Extraction de connaissances à partir des données

<small>Diapo Introduction</small>

Aussi appelé **Data Mining**

Plusieurs définitions proposées.

Terme apparu au début des années 90 (conference fondatrice en 1993).

* Dans de grands volumes de données :
  * > Extraction d'informations originales auparavant inconnues, potentiellement utile
  * > Découverte de nouvelles corrélations, tendances et modèles
  * > Processus d’aide à la décision en cherchant des modèles d’interprétation des données

Pourquoi ?

* Gros volumes de données collectées
  * Toutes les transactions sont enregistrées
  * Domaine commercial : Besoin de pouvoir exploiter ces informations, afin de faire evoluer les entreprises (nouveaux services...)
    * Remplace les processus d'enquête
  * Domaine scientifique : analyse de données
    * Astronomie
    * Biologie : analyse ADN -> observer comment les gènes se comportent dans une cellule données.

**But** : transformer les données en **connaissances**, pour

* Comprendre des phénomènes complexes : bioinformatique, chimie, environnement
* Mieux connaître ses clients
* Résister à la concurence
* Détecter des tentatives de fraudes, d'intrusions

## Domaine multidisciplinaire

<small>Diapo 6</small>

Une des technologies émergentes du XXIe siècle

![graphique](resources/graphique_domaine_multidisciplinaires.png)

Au croisement de :

* Statistiques
* Apprentissage artificiel, reconnaissance des formes
* Visualisation
* Base de données
  * Nouvelles modèlisations adaptées à la fouille de données, diffèrent des modèles opérationnelles normalisées.
  * Entrepot de données alimenté à partir du système d'information opérationnelle.

## Caractéristiques des problèmes de data mining

* Dimenseinons : plusieurs millions de cas, plusieurs centaines de variables
* Variables de différents types : numériques, textuelles
* Données présentes dans les SI des entreprises (pas de questionnaires particuliers ...)
* Données imparfaites (données manquantes...)
* Données évolutives
* Peu d'hypothèses statistiques sur la distribution des données (hypothèses experimentales)
* Nécessité de pouvoir déployer le modèle dans l'entreprise : faisabilité, explicabilité, ...
  * Les résultats doivent être explicables
    * Exemples des banques : demande de prêts -> informations saisies dans un logiciel, qui à partir des prêts accordés auparavant, donne un score pour l'accord du prêt.

## Domaines d'application

* Activités commerciales : grande distribution, ventre par connrespondance, banque, assurance
  * Segmentation de la clientèle : trouver des groupes au comportement similaires
  * Détermination du profil du consommateur
  * Analyse du panier de la ménagère : dans un caddy, quels produits vont ensemble
  * Mise au point de stratégies de rétention de la clientèle
  * Détection des fraudes (assurances)
  * Indentification de clients risques (demande de prêts)
* Activités industirelles
  * Détection et diagnostic de pannes et de défault
  * Analyse des flux dans les réseux de distribution
* Activités scientifiques
  * Diagnostic médical

## Processus d'EXD

### Poser le problème

Exemple : construire un outil de reconnaissance des mails de Spam

### Recherche des données

* Données existantes ou à constituer
  * Entrepôt de données, magasin de données
* Echantillon ou travail sur toutes les données
  * Performance ?

### Nettoyage des données

* Doublons, erreurs de saisie
* Valeurs aberrantes
* Informations manquantes
* Données dans diffèrents modèles suites à des fusions

Important pour fiabiliser les données

### Codage des données

* Regroupement
* Codage des attributes discrtes
* Changements de type
* Uniformisation déchelle
* Introduction de nouvelles variables

### Deux grandes problèmatiques

Deux méthodes : descriptif et prédictif.

Toutes les variables jouent le même rôle (pas de variable cible comme c'est le cas en data mining supervisé)

#### Data mining **descriptif**

 Comprendre les données en mettant en évidence des informations (motifs, structures, ..)
présentes dans les données -> aider l'humain à comprendre les données

* Grouper les exemples d'après leurs ressemblances -> **Clustering**
  * Apprentissage non supervisé : les données ne sont pas classées, on isole des sous-groupes d'enregistrements similaires les uns aux autres
  * Ex :constituer des ggroupes de clients ayant les me types de consommation de leur forfait téléphonique
* Chercher les motifs fréquentes dans les données -> **Recherche d'associations**

#### Data mining **prédictif** ou supervisé

L'objectif est de construire un modèle de décision à partir des exemples du passé.

* Prédire l'appartenance d'un objet à une classe
  * Ex : prédire si un mail est un spam
* Prédire la valeur d'une variable en fonction des autres variables
  * Ex : prédire les zones d'extension d'un feu de forêt, prédire le prix d'un jeu vidéo d'occasion

**Une variable cible** (la classe, la valeur à estimer) à expliquer grâce aux autres variables dites **explicatives** : on dira que l'on construit un modèle prédictif.

Techniques prédictives :

* Arbre de décision
* Réseaux de neurones
* Machines à vecteurs supports

**La classification** : affecter un objet à une classe en fonction de ses caractèristiques

Exemples:

* Diagnostic: risque de panne dans un système (2 classes);diagnostic médical ; diagnostic de cancers(ex : Mammaprint)
* Déterminer si un demandeur de prêt est un client « à risques » ou non
* Reconnaissance de chiffres manuscrits (10 classes)

#### Supervisé vs Non supervisé

Le data mining _prédictif_ est une tâche supervisée : Les données contiennent une variable cible dont la valeur est connue dans les données étudiées: cette valeur a pu être obtenue par un « superviseur »

Le data mining _explicatif_ est une tâche non supervisée : toutes les variables jouent le même rôle.