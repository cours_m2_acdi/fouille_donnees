module.exports = {
    title: 'M2 ACDI Fouille de données',
    description: '',
    base: '/fouille_donnees/',
    dest: 'public',
    themeConfig: {
        nav: [
          { text: 'Intro', link: '/' },
        ],
        sidebar: [
            '/',
            '/format_donnees/',
            '/arbre_decision/'
        ]
      }
}